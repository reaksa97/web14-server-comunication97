
import './App.css';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import HomePage from './pages/HomePage';
import UserPage from './pages/UserPage';
import ProductPage from './pages/ProductPage';
import NotFoundPage from './pages/NotFoundPage';
import 'bootstrap/dist/css/bootstrap.min.css'; 
import ViewUserProfile from './pages/ViewUserProfile';
import MyNavBar from './components/MyNavBar';


function App() {
  

  return (
    
      <BrowserRouter>
      <MyNavBar/>
      <Routes>
        <Route path='/'index element={<HomePage/>}/>
        <Route path='/user' index element={<UserPage/>}/>
        <Route path='/product' index element={<ProductPage/>}/>
        <Route path='/user/:id' element={<ViewUserProfile/>} />
        <Route path='*' index element={<NotFoundPage/>}/>
      </Routes>
      </BrowserRouter>
    
  );
}

export default App;

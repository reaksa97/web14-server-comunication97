import axios from 'axios'
import React, { useEffect, useState } from 'react'
import { GET_ALL_USERS } from '../service/UserService'
import UserCard from '../components/UserCard'
import PlaceholderCard from '../components/PlaceholderCard'

function UserPage() {
   const [users,setusers]=useState([])
   const [isLoading,setisLoading]=useState(true)
   useEffect(()=>{
     GET_ALL_USERS()
     .then(response=>{
      setusers(response)
      setisLoading(false)
     })
     .catch(error=>{
      console.log("Erorr:",error)
      setisLoading(false)
    })
   },[])
   console.log("all user:",users)
   const renderPlaceholderCard = (numberCard) =>{
    let allcards=[]
      for(var i=0;i<numberCard;i++){
       allcards.push(
        <div className="col-4 d-flex justify-content-center">
        <PlaceholderCard/>
        </div>
       )
      }
      return allcards;

   }
   
  return (
    <div className='container  mt-5'>
      <h1 className='text-center'>Userpage</h1>
      <div className="row">
       {
        // users.map((user)=>(
        //   <div className='col-4 d-flex justify-content-center mt-1'>
          
        //       <UserCard userInfo={user}/>
        //   </div>
        // ))


        
       }
       {
        isLoading ?
        (
           <>
           {renderPlaceholderCard(6)}
           </>
        ):
        (
         <>
            {
              users.map((user)=>(
                <div className='col-4 d-flex justify-content-center mt-1'>
                
                    <UserCard userInfo={user}/>
                </div>
              ))
            }
         </>
        )
       }
      </div>

    </div>
  )
}

export default UserPage
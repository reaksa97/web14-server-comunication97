import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'
import { GET_ALL_BY_ID } from '../service/UserService'

function ViewUserProfile() {
   const {id}= useParams()
   
   const [user,setUser]=useState({})
   useEffect (()=>{
    GET_ALL_BY_ID(id)
    .then(response=>setUser(response))
    .catch(error=>console.log("error"))
   },[])
  return (
    <div className='container mt-5 bg-light py-5 px-3'>
      
      <div className="d-flex">
    
        <img src={user.avatar} alt="" />
        <div className='ms-5 ps-2 mt-4'>
          <h1>infomation</h1>
          <br />
          <h3>Name:{user.name}</h3>
          <p>Role:{user.role}</p>
          <p>Email:{user.email}</p>
          <button className='btn btn-warning'>Update</button>

        </div>
      </div>

     
    </div>
    
  )
}

export default ViewUserProfile
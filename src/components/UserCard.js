import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import { NavLink } from 'react-router-dom';

function UserCard({ userInfo}) {
  return (
    <Card style={{ width: '18rem' }}>

       <Card.Img variant="top" src={userInfo.avatar?userInfo.avatar : "https://media.istockphoto.com/vectors/person-gray-photo-placeholder-man-vector-id1152265845?k=6&m=1152265845&s=170667a&w=0&h=2nC1q_2Lm_ETu5bG-ryE2OGMS0LPP838j8XC70gx6sk="}
       onError={(e)=>{
        e.target.onError=null;
        e.target.src="https://media.istockphoto.com/vectors/person-gray-photo-placeholder-man-vector-id1152265845?k=6&m=1152265845&s=170667a&w=0&h=2nC1q_2Lm_ETu5bG-ryE2OGMS0LPP838j8XC70gx6sk="
       }} />
      <Card.Body>
        <Card.Title>
         {userInfo.name}
        <span className='text-warning'>
         {userInfo.role}
        </span>
        </Card.Title>
        <Card.Text>
        {userInfo.email}
        </Card.Text>
       <NavLink to={`/user/${userInfo.id}`}>
       <Button variant="primary">Click On</Button>
       </NavLink>
      </Card.Body>
    </Card>
  );
}

export default UserCard;